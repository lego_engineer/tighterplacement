# Contributing
Thank you for your interest in contributing to this project!

## Features and Bugs
Should you experience any bugs, or have any feature wishes, please let us know by [creating an issue](https://gitlab.com/lego_engineer/tighterplacement/issues/new). 

Before creating an issue, please search for existing issues; the issue may have been reported or solved by someone else.

## Merge Requests

We encourage you to contribute code and we very much appreciate all help on making this a better project.
To stay on track we ask you to respect the following **contribution rules**:
- Fork the repository when working on a feature or bugfix.
- Create merge requests based on the `development` branch.
- Update the [`CHANGELOG`](/CHANGELOG.md) and [`README`](/README.md) files if relevant.

## Contact

Feel free to contact us if you have questions or feedback.


### Attribution

This contribution guide is adapted from the fairplay-zone/docker-dontstarvetogether [contribution guide](https://github.com/fairplay-zone/docker-dontstarvetogether/blob/develop/CONTRIBUTING.md)