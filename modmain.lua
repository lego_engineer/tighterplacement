-- Flower Settings
AddPrefabPostInit("butterfly", function(inst)
    if inst.components.deployable then
        if GetModConfigData("flower_spacing") ~= "Unset" then
          print("MOD INFO", GetModConfigData("flower_spacing"))
          spacing = GLOBAL.DEPLOYSPACING[GetModConfigData("flower_spacing")]
          print("MOD INFO", spacing)
          inst.components.deployable:SetDeploySpacing(spacing)
        end
    end
end)