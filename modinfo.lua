name = "Tighter Placement"
description = "A mod to adjust item spacing."
author = "lego_engineer"
version = "1.0.0"
forumthread = ""

-- GitLab Repo: https://gitlab.com/lego_engineer/tighterplacement

api_version_dst = 10
dont_starve_compatible = false
reign_of_giants_compatible = false
dst_compatible = true
all_clients_require_mod = true 

icon_atlas = "modicon.xml"
icon = "modicon.tex"

server_filter_tags = {
"utility",
"spacing"
}

configuration_options =
{
	{
		name="flower_spacing",
		label="Flower Spacing",
		hover = "The spacing to use between each flower.\n A tile is four spaces wide.",
		options = {
      {data="Unset", description="Default"},
			{data="NONE", description="No Spacing (0)"},
			{data="LESS", description="Tiny (0.75)"},
			{data="MEDIUM", description="Small (1)"},
			{data="DEFAULT", description="Medium (2)"},
			{data="PLACER_DEFAULT", description="Large (3.2)"},
		},
		default="Unset"
	},
	
}