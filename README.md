# Don't Starve Tighter Placement Mod
I thought flowers were too spaced out. So I made a mod to modify their spacing. 
Other things may be added as time goes on.

# Controllable Spacings
* Flowers

## Contribution
Do you want to contribute to the project? Check out the [contribution guide](/CONTRIBUTING.md).
